@extends('layouts.app')
@section('htmlheader_title')
Perfil
@endsection
@section('main-content')
<section class="content-header">
	<h1 style="height: 25px;margin-bottom: 5px">
	<legend><i class='fa fa-user'></i>  Perfil</legend>
	<small>@yield('contentheader_description')</small>
	</h1>
</section>
<div class="panel-default">
	<div class="panel panel-body">
		<div class="col-md-12">
			<form id="f_edit_perfil"  class="update_perfil" action="{{url('/edit_perfil')}}" method="post" >
				<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Nombre</label>
						<input type="text" class="form-control" value="{{$data[0]->name}}" autocomplete="off" name="name">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Correo</label>
						<input type="text" class="form-control" value="{{$data[0]->email}}" id="searchMail" autocomplete="off" oncopy="return false;" onpaste="return false;" oncut="return false;" name="email">
						<p id="msj_mailA" style="color: #a94442"></p>
					</div>
				</div>
				<div class="col-md-6 col-md-offset-3">
					<button class="btn btn-success btn-block" >Actualizar</button>
				</div>
			</form>
		</div>
		
		
		<legend>Actualizar contraseña</legend>
		<div class="col-md-12">
			<form id="password_edit"  class="form_perfil" action="{{url('/cambiar_password')}}" method="post" >
				<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
				<div class="col-md-6">
					<div class="form-group">
						<label>Nueva Contraseña</label>
						<input type="password" class="form-control" name="password" placeholder="****************" id="newpassword">
					</div>
					<p id="msj_pass1" style="color:#a94442"></p>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Repetir Contraseña</label>
						<input type="password" class="form-control" name="password_confirmation"  placeholder="****************" id="password_confirmation">
					</div>
					<p id="msj_pass" style="color:#a94442"></p>
				</div>
				<div class="col-md-6 col-md-offset-3">
					<button class="btn btn-success btn-block" id="subContra">Actualizar</button>
				</div>
			</form>
			
		</div>
		
	</div>
</div>
@endsection