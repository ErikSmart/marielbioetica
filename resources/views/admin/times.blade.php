@foreach($config as $cof)
<div class="container" style="width: 100%;">
	<form id="form_time" class="form_times"  action="{{url('/dataTimes')}}" method="POST" autocomplete="off">
		<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
		<div class="col-sm-12">
			<div class="form-group">
			<label>Hora de Adelanto</label>
			<div class="input-group">		
				<input type="number" max="10" min="0" value="{{$cof->hour}}" name="hour" class="form-control">
				<span class="input-group-addon">Hora</span>
			</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
			<label>Minutos de Adelanto</label>
			<div class="input-group">
				<input type="number" max="60" min="0" value="{{$cof->minute}}" name="minute" class="form-control">
				<span class="input-group-addon">Min.</span>
			</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
				<label>Establece la zona horaria</label>
				<p>Mas info en <a href="http://php.net/manual/es/timezones.php"  target="_blank">Manual PHP <i class="fa fa-link"></i></a>  </p>
				<input type="text" name="zona" class="form-control" value="{{$cof->zona}}">
			</div>
		</div>
		<div class="col-sm-12">
			<button type="submit" class="btn btn-float btn-block btn-success"> Guardar Cambios</button>
		</div>
	</form>
</div>
@endforeach