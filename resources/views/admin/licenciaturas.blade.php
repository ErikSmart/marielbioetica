@extends('layouts.app')
@section('htmlheader_title')
Licenciaturas
@endsection
@section('main-content')
<section class="content-header">
	<h1 style="height: 25px;margin-bottom: 5px">
	<legend><i class='fa fa-university'></i>  Listado Licenciaturas agregadas</legend>
	<small>@yield('contentheader_description')</small>
	</h1>
</section>
<div class="container">
	<div class="panel-default">
		<div class="panel panel-body">
			<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
			<table id="Lic" class="table-condensed table-hover">
				<thead>
					<tr>
						<th class="col-md-4 col-xs-4">
							Nombre
						</th>
						<th class="col-md-4 col-xs-4">
							Area
						</th>
						<th class="col-md-1 col-xs-4">
							Color
						</th>
						<th class="col-md-1 col-xs-4">
							Editar
						</th>
						<th class="col-md-1 col-xs-4">
							Eliminar
						</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div id="ModalLIC" class="modal fade" role="dialog">
		<div class="modal-dialog  modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
					<h4 class="modal-title text-center">Editar Formación Licenciatura <i class="fa fa-pencil text-warning"></i></h4>
				</div>
				<div class="modal-body">
					
					<div id="contenido"></div>
					
					
				</div>
				
			</div>
		</div>
	</div>
</div>
@endsection