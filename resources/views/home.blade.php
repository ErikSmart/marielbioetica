﻿@extends('layouts.app')
@section('htmlheader_title')
Listado de Usuarios
@endsection
@section('main-content')
<section class="content-header">
    <h1 style="height: 25px;margin-bottom: 5px">
        <legend><i class='fa fa-users'></i>  Listado de Usuarios</legend>
        <small>@yield('contentheader_description')</small>
    </h1>
</section>
<div class="container">
	<div class="panel panel-default" >
		<div class="panel-body">
			<div class="table-responsive">
				<table  id="ListadoUsers" class="table-condensed table-hover">
					<thead>
						<tr>
							<th class="col-md-4 col-xs-4">Nombre Completo</th>
							<th class="col-md-3 col-xs-4">Correo</th>
							<th class="col-md-3 col-xs-4">Telefono</th>
							<th class="col-md-3 col-xs-4">Perfil</th>
							<th class="col-md-4 col-xs-4"><center>Estado</center></th>
			
							<th>Detalles</th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($listado as $l)
						<tr>
							<td>{{$l->nombreCompleto}}</td>
							<td>{{$l->correo}}</td>
							<td>{{$l->telefono}}</td>
							<td>{{$l->perfil}}</td>
							
							@if($l->idUser == NULL)
								<td style="background: #fff;color:#FF4F4F">	<h4 align="center">Pendiente</h4></td>
							@else
								<td style="background: #99FB72;color:#000"><h4 align="center">Aceptado</h4></td>
							@endif

			
							<td>
								<!-- ===================== Inicio Botones de editar y eliminar ========================== -->
								<center><button onclick="fichaUsuario({{$l->idV}});" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Ver Datos de Usuario"><i class="fa fa-user" aria-hidden="true"></i></button></center>
							</td>
							
							<td>
								
								@if($l->deleted_at == NULL && $l->status == 'Activo' && $l->idUser != NULL)
								<button  class="btn" onclick="alta({{$l->idV}})" data-toggle="tooltip" data-placement="top" title="Dar de Baja" style="background: #FF622E;color:#fff"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></button>
								@elseif($l->deleted_at == NULL && $l->status == 'Inactivo' &&  $l->idUser != NULL)
								<button  class="btn btn-primary " onclick="activo({{$l->idV}})" data-toggle="tooltip" data-placement="top" title="Reincorporar"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
								@endif
								
							</td>
							<td>
								<button onclick="eliminar({{$l->idV}});" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar datos"><i class="fa fa-trash" aria-hidden="true"></i></button>
								
							</td>
							
							<!-- ===================== Fin Botones de editar y eliminar ========================== -->
							
							
							<!-- =============================== Inicio dar de baja a Usuario ============================================ -->
							<td>
								
								@if($l->deleted_at == NULL && $l->idUser != NULL)
								
								<button onclick="desactivar({{$l->idUser}});" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Desactivar Sesión"><i class="fa fa-lock" aria-hidden="true"></i></button>
								@elseif($l->idUser != NULL)
								
								<button onclick="activar({{$l->idUser}});" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Activar Sesión"><i class="fa fa-unlock" aria-hidden="true"></i></button>
								@endif
								
							</td>
							
							<!-- ================================ Fin dar de baja a Usuario =========================================== -->
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</div>

@endsection