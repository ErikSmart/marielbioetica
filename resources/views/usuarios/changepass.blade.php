<div class="panel-default">
	<div class="panel panel-body">
		<legend>Actualizar contraseña</legend>
		<form id="password_edit"  class="form_change" action="{{url('/cambiar_password')}}" method="post" >
		  <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
			<div class="col-md-12">
	    		<div class="col-md-6">
	    			<div class="form-group">
	    			<label>Nueva Contraseña</label>
	    			<input type="password" class="form-control" name="password" placeholder="****************" id="newpassword">
	    			</div>
	    			<p id="msj_pass1" style="color:#a94442"></p>		
	    		</div>
	    		<div class="col-md-6">
	    			<div class="form-group">
	    			<label>Repetir Contraseña</label>
	    			<input type="password" class="form-control" name="password_confirmation"  placeholder="****************" id="password_confirmation">
	    			</div>
	    			<p id="msj_pass" style="color:#a94442"></p>
	    		</div>
    		<div class="col-md-6 col-md-offset-3">
	    			<button class="btn btn-success btn-block" id="subContra">Actualizar</button>
	    	</div>
	    	</div>
		</form>
	</div>
</div>