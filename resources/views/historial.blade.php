﻿@extends('layouts.app')
@section('htmlheader_title')
Historial
@endsection
@section('main-content')
<section class="content-header">
	<h1 style="height: 25px;margin-bottom: 5px">
	<legend><i class='fa fa-users'></i>  Listado de Usuarios</legend>
	<small>@yield('contentheader_description')</small>
	</h1>
</section>
<div class="container">

<div class="panel panel-default" >
		<div class="panel-body">
			<div class="table-responsive">
				<table  id="ListadoUsers" class="table-condensed table-hover">
					<thead>
						<tr>
							<th class="col-md-3 col-xs-4">Autor</th>
							<th class="col-md-3 col-xs-4">Acción</th>
							<th class="col-md-3 col-xs-4">Echo a</th>
							<th class="col-md-3 col-xs-4">Fecha de Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($historial as $l)
						<tr>
							<td>{{$l->name}}</td>
							<td>{{$l->action}}</td>
							<td>{{$l->nombreCompleto}}</td>
							<td>{{$l->fecha}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection