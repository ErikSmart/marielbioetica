<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body class="skin-yellow sidebar-mini">
<div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            @yield('main-content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show
<!-- DETALLES DE USUARIOS -->
<div id="ModalDetalles" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title text-center">Datos Completos </h4>
            </div>
            <div class="modal-body">
                
                <div id="datosUsuario"></div>
                
                
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-success"  id="aceptarU" onclick="validarUser()">Aceptar Usuario</button>
              </div>
        </div>
    </div>
</div>
<!-- CONFIGURAR DATOS DE ZONA HORARIA -->
<div id="ModalTimes" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title text-center">Configurar Hora </h4>
            </div>
            <div class="modal-body">
                
                <div  id="viewTimes"></div>
                
                
            </div>
             <div class="modal-footer">
                
              </div>
        </div>
    </div>
</div>
</body>
</html>
