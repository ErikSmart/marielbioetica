<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>
<!-- scroll con js -->
<script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- datatables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<!-- aqui se encuatran declaradas la tablas -->
<script src="{{ asset('/js/tablas.js')}}" ></script>
<!-- cargador automatico -->
<script src="{{asset('/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<!-- funciones de modales -->
<script src="{{asset('/js/modales.js')}}"></script>
 <!--   jquery-confirm -->
<script src="{{ asset('/js/jquery-confirm.js ')}}"></script>
<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- selct2 -->
<script src="{{ asset('/plugins/select2/select2.min.js') }}"></script>
<!-- dar de alta o reincoporar el usuario -->
<script src="{{ asset('/js/accionesUser.js')}}"></script>
<!-- envio de formularios -->
<script src="{{ asset('/js/sendformu.js')}}"></script>
<!-- cargar nuevos regitros -->
<script src="{{ asset('/js/admin/Loadatadmin.js')}}"></script>
<script src="{{ asset('/js/admin/perfil.js')}}"></script>
<script src="{{ asset('/js/admin/updateLic.js')}}"></script>

 <script>
   $(function () {
        $('input').iCheck({

            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
       
    });

	$(".js-example-tokenizer").select2({
      tags: true,
      tokenSeparators: [',', ' ']
    })

  
   $(".Licenciatura").select2({
     placeholder: "seleccione su Licenciatura" 
   });

</script>
