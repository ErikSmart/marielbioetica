<?php  
$mysqli = new mysqli("192.168.21.205", "forge", "", "bioetica");
if ($mysqli->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}
//echo $mysqli->host_info . "\n";
$query="select user.*,user.correo,case when user.correo is null then 'No inscrito' else 'Inscrito' end as inscritoMail,nombre_miembro,mail_miembro,titulo_miembro,generacion_miembro,
case when m.inscrito='Inscrito' or user.correo is not null then 'Inscrito' else 'No inscrito' end as Inscritoreal
from(
select miem.*,case when user.idsiu_user is null then 'No inscrito' else 'Inscrito' end as inscrito
from bioetica.miembrosbio miem
LEFT OUTER JOIN bioetica.usercomplete user on(miem.idsiu_miembro=user.idsiu_user)
) m LEFT OUTER JOIN bioetica.usercomplete user on(m.mail_miembro=user.correo  and user.correo <>'' and m.mail_miembro<>'')
order by id desc";
$result = $mysqli -> prepare($query);
$result -> execute();
$result -> store_result();
$resultado = $mysqli->query($query);
header('Content-Type: text/html; charset=UTF-8');
header('Content-Type: application/vnd.ms-excel'); // This should work for IE & Opera 
header('Content-type: application/x-msexcel'); // This should work for the rest 
header('Content-Disposition: attachment; filename="Comparativo inscrito/no inscritos Bioetica.xls"');
?>

				<table  id="ListadoUsers" class="table-condensed table-hover">
					<thead>
						<tr>
							<th class="col-md-3 col-xs-4">Nombre</th>
							<th class="col-md-3 col-xs-4">Correo</th>
							<th class="col-md-3 col-xs-4">Descripci&oacute;n</th>
							<th class="col-md-3 col-xs-4">Generaci&oacute;n</th>
							<th class="col-md-3 col-xs-4">Carrera</th>
							<th class="col-md-3 col-xs-4">&Aacute;rea</th>
							<th class="col-md-3 col-xs-4">Maestr&iacute;a</th>
							<th class="col-md-3 col-xs-4">Doctorado</th>
							<th class="col-md-3 col-xs-4">Temas Investigaci&oacute;n</th>
							<th class="col-md-3 col-xs-4">Tel&eacute;fono</th>
							<th class="col-md-3 col-xs-4">Responsabilidades</th>
							<th class="col-md-3 col-xs-4">Resp. Doctorado</th>
							<th class="col-md-3 col-xs-4">Relaci&oacute;n</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($result -> num_rows >= 1)
				        {
					          while ($fila = $resultado->fetch_assoc()) {
					        //printf ("%s (%s)\n", $fila["idsiu_miembro"], $fila["mail_miembro"]);
					             //Der User wird zum Benutzer-Interface weiter geleitet
					          	if($fila['id']){
							        ?>
									<tr>
										<td><?php echo utf8_decode($fila['nombreCompleto']);?></td>
										<td align="center"><?php echo $fila['correo'];?></td>
										<td align="center"><?php echo utf8_decode($fila['descripcion']);?></td>
										<td align="center"><?php echo utf8_decode($fila['nombre_carrera']);?></td>
										<td align="center"><?php echo $fila['generacion_miembro'];?></td>
										<td align="center"><?php echo utf8_decode($fila['nombreArea']);?></td>
										<td align="center"><?php echo utf8_decode($fila['formdeMaestria']);?></td>
										<td align="center"><?php echo utf8_decode($fila['formDoctorado']);?></td>
										<td align="center"><?php echo utf8_decode($fila['investigacion_temas']);?></td>
										<td align="center"><?php echo $fila['telefono'];?></td>
										<td align="center"><?php echo utf8_decode($fila['resGes1']." ".$fila['resGes2']." ".$fila['resGes3']);?></td>
										<td align="center"><?php echo utf8_decode($fila['resDoc1']." ".$fila['resDoc2']." ".$fila['resDoc3']);?></td>
										<td align="center"><?php echo utf8_decode($fila['relacion']);?></td>
									</tr>
							        <?php
							    	}else{
								        ?>
										<tr>
											<td><?php echo utf8_decode($fila['nombre_miembro']);?></td>
											<td align="center"><?php echo $fila['mail_miembro'];?></td>
											<td align="center"><?php echo utf8_decode($fila['titulo_miembro']);?></td>
											<td align="center"><?php echo $fila['generacion_miembro'];?></td>
											<td colspan="9" align="center"><?php echo $fila['Inscritoreal']; ?></td>
										</tr>
								        <?php
								    }
					    	}
				         
				        }else
				        {
				          $result -> close();
				          echo "<br>Anmeldung fehlgeschlagen";
				        }
				        ?>
					</tbody>
				</table>