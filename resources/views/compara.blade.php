@extends('layouts.app')
@section('htmlheader_title')
Compara
@endsection
@section('main-content')
<?php  
$mysqli = new mysqli("192.168.21.205", "forge", "", "bioetica");
if ($mysqli->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}
//echo $mysqli->host_info . "\n";
$query="select m.*,user.correo,case when user.correo is null then 'No inscrito' else 'Inscrito' end as inscritoMail,
case when m.inscrito='Inscrito' or user.correo is not null then 'Inscrito' else 'No inscrito' end as Inscritoreal
from(
select miem.*,case when user.idsiu_user is null then 'No inscrito' else 'Inscrito' end as inscrito
from bioetica.miembrosbio miem
LEFT OUTER JOIN bioetica.usercomplete user on(miem.idsiu_miembro=user.idsiu_user)
) m LEFT OUTER JOIN bioetica.usercomplete user on(m.mail_miembro=user.correo  and user.correo <>'' and m.mail_miembro<>'')
";
$result = $mysqli -> prepare($query);
$result -> execute();
$result -> store_result();
$resultado = $mysqli->query($query);
?>
<section class="content-header">
	<h1 style="height: 25px;margin-bottom: 5px">
	<legend><i class='fa fa-users'></i>  Relación de Inscritos y no Inscritos</legend>
	<small>@yield('contentheader_description')</small>
	</h1>
</section>
<div class="container">

<div class="panel panel-default" >
		<div class="panel-body">
			<div class="table-responsive">
				<div><a href="{{ url('comparaexcel') }}" target="_blank"><img src="http://suas-emc2.anahuac.mx/bio/resources/img/excel.png" width="35" height="35"></a></div>
				<table  id="ListadoUsers" class="table-condensed table-hover">
					<thead>
						<tr>
							<th class="col-md-3 col-xs-4">Nombre</th>
							<th class="col-md-3 col-xs-4">Correo</th>
							<th class="col-md-3 col-xs-4">Estatus Área</th>
							<th class="col-md-3 col-xs-4">Estatus Sistema</th>
							<th class="col-md-3 col-xs-4">Generación</th>
							<th class="col-md-3 col-xs-4">Enviar Correo</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($result -> num_rows >= 1)
				        {
					          while ($fila = $resultado->fetch_assoc()) {
					        //printf ("%s (%s)\n", $fila["idsiu_miembro"], $fila["mail_miembro"]);
					             //Der User wird zum Benutzer-Interface weiter geleitet
					        ?>
							<tr>
								<td><?php echo $fila['nombre_miembro'];?></td>
								<td><?php echo $fila['mail_miembro'];?></td>
								<td><?php echo $fila['titulo_miembro'];?></td>
								<td><?php echo $fila['Inscritoreal'];?></td>
								<td><?php echo $fila['generacion_miembro'];?></td>
								<td>
									<?php
									if($fila['Inscritoreal']=='Inscrito' || !$fila['mail_miembro'])
									{
										echo "No enviar";
									}else{
										?>
										<input type="checkbox" name="enviaCorreo" class="EnviaCorreo" id="EnviaCorreo" value="<?php echo $fila['mail_miembro'];?>">
										<?php
									}
									?>

								</td>
							</tr>
					        <?php
					    	}
				         
				        }else
				        {
				          $result -> close();
				          echo "<br>Anmeldung fehlgeschlagen";
				        }
				        ?>
					</tbody>
				        <tr>
				        	<td colspan="5" align="center"><input type="button" name="EnviarMails" value="Enviar Mails" class="button enviaMail" onClick="gdaConPerPrue('popup-1');"></td>
				        </tr>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="popup" data-popup="popup-1">
<div class="popup-inner">
<div style="background-color: #FF8D33;"><h2 align="center">Mailing Bioética</h2></div>
<div class="mandalo">Prueba</div>
<a class="popup-close" data-popup-close="popup-1" type="button" onClick="cierraPop('popup-1')" href="#">x</a>
</div>
</div>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
	/*function gdaConPerPrue(popup)
	{
		//alert("Prueba");
		var targeted_popup_class = popup;
		//alert(targeted_popup_class);
		$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
	}*/
	//----- CLOSE
	/*function cierraPop(popupC) {
		var targeted_popup_class= popupC;
		$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		e.preventDefault();
		//alert("popo");
	}*/

	$(document).ready(function() {
    	$(".enviaMail").on("click", function(){
			 var targeted_popup_class = 'popup-1';
			 var correos;
			$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
			$("input:checkbox:checked").each(function() {
				correos=correos+ "," +$(this).val();
             //alert($(this).val());
        	});
        	//alert(correos);
        	$.post( 
                  "envia.php",
                  { name: "Zara" },
                  function(data) {
                     $('.mandalo').html(data);
                  }
               );
		});
    	$(".popup-close").on("click", function(){
			 var targeted_popup_class = 'popup-1';
			$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		});
	});


</script>

<style type="text/css">
	/* Outer */
.popup {
width:100%;
height:100%;
display:none;
position:fixed;
top:0px;
left:0px;
background:rgba(0,0,0,0.75);
}
/* Inner */
.popup-inner {
max-width:1500px;
width:90%;
height: 700px;
position:absolute;
top:50%;
left:55%;
-webkit-transform:translate(-50%, -50%);
transform:translate(-50%, -50%);
box-shadow:0px 2px 6px rgba(0,0,0,1);
border-radius:3px;
background:#fff;
}
/* Close Button */
.popup-close {
width:30px;
height:30px;
padding-top:4px;
display:inline-block;
position:absolute;
top:0px;
right:0px;
transition:ease 0.25s all;
-webkit-transform:translate(50%, -50%);
transform:translate(50%, -50%);
border-radius:1000px;
background:rgba(0,0,0,0.8);
font-family:Arial, Sans-Serif;
font-size:20px;
text-align:center;
line-height:100%;
color:#fff;
}
.popup-close:hover {
-webkit-transform:translate(50%, -50%) rotate(180deg);
transform:translate(50%, -50%) rotate(180deg);
background:rgba(0,0,0,1);
text-decoration:none;
}
	/* Outer */
.popup2 {
width:100%;
height:100%;
display:none;
position:fixed;
top:0px;
left:0px;
background:rgba(0,0,0,0.75);
}
/* Inner */
.popup2-inner {
max-width:700px;
width:90%;
padding:40px;
position:absolute;
top:50%;
left:50%;
-webkit-transform:translate(-50%, -50%);
transform:translate(-50%, -50%);
box-shadow:0px 2px 6px rgba(0,0,0,1);
border-radius:3px;
background:#fff;
}
/* Close Button */
.popup2-close {
width:30px;
height:30px;
padding-top:4px;
display:inline-block;
position:absolute;
top:0px;
right:0px;
transition:ease 0.25s all;
-webkit-transform:translate(50%, -50%);
transform:translate(50%, -50%);
border-radius:1000px;
background:rgba(0,0,0,0.8);
font-family:Arial, Sans-Serif;
font-size:20px;
text-align:center;
line-height:100%;
color:#fff;
}
.popup2-close:hover {
-webkit-transform:translate(50%, -50%) rotate(180deg);
transform:translate(50%, -50%) rotate(180deg);
background:rgba(0,0,0,1);
text-decoration:none;
}
</style>
@endsection