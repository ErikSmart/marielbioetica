@extends('layouts.landing')<!--se pone nombre_de_carpeta.archivo-->
@section('content')
<link rel="stylesheet" href="{{asset('/css/inicio.css')}}">
@include('cuerpo.footerRelativo')
<body class="unete">
    <div class="container">
        <br>
        <form id="form_usuario" class="form_entrada" action="{{url('/newUsuario')}}" method="post" >
            <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
            <div class="col-lg-8 col-lg-offset-2 col-md-10">
                <div class="panel panel-default">
                    <div class="panel-body">
                      <center><h3><b>RED DE VINCULACIÓN DE EGRESADOS FACULTAD DE BIOÉTICA UNIVERSIDAD ANÁHUAC MÉXICO NORTE</b></h3></center>
                      <p>La información proporcionada será colocada en una página web disponible al público abierto. Esto te permitirá conocer el trabajo de otros, y otros podrán conocer tu trabajo y trayectoria, para formar una plataforma de vínculos que fomenten la suma de esfuerzos y evite trabajos fútiles y repetitivos en el área de la Bioética. !BIENVENIDO, EN UNOS MINUTOS FORMARÁS PARTE DE NUESTRA RED!</p>

                        <div class="form-group" id="nombre">
                          <label>Nombre completo *</label>
                          <p>Coloque su nombre completo</p>
                            <input type="text" class="form-control id_nombre" name="nombreCompleto" onkeyup="msjclear('msj_id_nombre');" autocomplete="off">
                           <p style="display: none;color: #a94442" id="msj_id_nombre"></p>

                        </div>

                        <label>Relación actual con la Facultad de Bioética U. A. *</label>
                        <p>Seleccione el aplique</p>
                        <div id="id_perfil_error"> </div>
                            @foreach($perfil as $p)
                            <div class="form-group ">
                                <input type="radio" name="id_perfil"  value="{{$p->id}}" class="id_perfil" >&nbsp;&nbsp;{{$p->descripcion}}
                            </div>
                            @endforeach
                            <p style="display: none;color: #a94442" id="msj_perfil"></p>


                        <div class="form-group">
                            <label>Grado(s) que obtuvo en la Facultad de Bioética U.A. y su status actual (INCOMPLETA, COMPLETA, o TITULADO) )</label>
                            <p>"Incompleta"= actualmente estudiando, "Completa"= Créditos completos sin título, "Titulado" = ya presentó examen de título. P.e."Maestría en Bioética Titulado y Doctorado en Bioética en Curso".</p>
                            <!-- <div id="status_formacion"> </div> -->
                              {{--    @foreach($statusF as $sf)
                            <div class="form-group">
                                <input type="radio" name="id_status_formacion"  value="{{$sf->id}}" class="id_status_formacion" >&nbsp;&nbsp;{{$sf->descripcion}}
                            </div>
                            @endforeach --}}
                            <input type="text" class="form-control id_status_formacion" name="relacion"> <!-- onkeyup="msjclear('msj_id_status_formacion');" required="">
                           <p style="display: none;color: #a94442" id="msj_id_status_formacion"></p> -->

                        </div>
                        <div class="row">

                        <div class="row ">
                        <br>

                        <div class="col-lg-9 col-xs-12" style="padding-left: 33px;">
                           <label>Formación Académica de LICENCIATURA *</label>
                        </div>
                        </div>
                        <div class="row"  style="padding-left: 33px;" >
                          <div class="col-lg-9 col-xs-12">
                            <p>Si tu licenciatura no se encuentra en la lista pulsa el botón agregar </p>
                          </div>
                          <div class="col-lg-2 col-xs-12">
                            <button type="button" class="btn-primary btn-block btn-sm" data-toggle="modal" data-target="#myModal" >Agregar</button>
                          </div>
                        </div>


                            <div class="col-lg-12">
                            <br>
                                <div class="form-group">

                                    <select class="Licenciatura form-control" name="formAcademicLIC">

                                    </select>
                                </div>
                           </div>

                        </div>
                       {{--  <div class="form-group">
                            <label>Identifique el área a la que corresponde su licenciatura de base</label>
                            <br>
                            <small><b>Seleccione el área con que MÁS IDENTIFIQUE su formación/licenciatura de base</b></small>
                            <div id="area_LIC"> </div>
                                @foreach($areasLic as $alic)
                                <div class="form-group">

                                    <input type="radio" name="id_area_LIC" class="id_area_LIC" value="{{$alic->id}}" >&nbsp;&nbsp;{{$alic->nombreArea}}
                                </div>
                                @endforeach

                            <p style="display: none;color: #a94442" id="msj_id_area_LIC"></p>

                        </div> --}}
                        <div class="form-group">
                            <label>Formación Académica de MAESTRÍA (A parte de la de Bioética)</label>
                            <p>Coloque el nombre de su maestría</p>
                            <input type="text" class="form-control" name="formdeMaestria" placeholder="Coloque el nombre de su maestría ejemplo: “Maestría en medicina”" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Formación Académica de ESPECIALIDAD (A parte de la de Bioética)</label>
                            <p>Si usted tiene alguna Especialidad Médica o Académica</p>
                            <input type="text" class="form-control" name="formEspecialidad" placeholder="Si usted tiene alguna Especialidad Médica o Académica ejemplo: “Especialidad en medicina”" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Formación Académica de DOCTORADO (A parte de la de Bioética)</label>
                            <p>Coloque el nombre de su doctorado</p>
                            <input type="text" class="form-control" name="formDoctorado" placeholder="Coloque el nombre de su doctorado ejemplo: “Doctorado en medicina”" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Otra formación de POSGRADO (A parte de la mencionada antes)</label>
                            <p>Si lo desea, coloque algún(os) otro(s) grado(s) académico(s) con los que cuenta</p>
                            <textarea  id="" cols="30" rows="10" name="formPostGrado" class="form-control" style="max-width: 100%;max-height: 200px" ></textarea>

                        </div>
                        <div class="form-group">
                            <label>Ámbito Gestión/Laboral en Bioética-STATUS *</label>
                            <p>STATUS ACTUAL: ACTIVO= su trabajo implica y se relaciona con la Bioética directamente. // INACTIVO= en su trabajo actual no hay relación directa con la Bioética PERO lo ha hecho antes. // INEXISTENTE= NUNCA se ha desempeñado laboralmente en proyectos que se relacionen con la Bioética directamente.</p>
                            <div id="status_gestion_laboral"></div>
                            @foreach($statusL as $sL)

                                <div class="form-group">

                                    <input type="radio" name="id_status_gestion_laboral"  class="id_status_gestion_laboral" value="{{$sL->id}}" >&nbsp;&nbsp;{{$sL->descripcion}}
                                </div>

                            @endforeach

                            <p style="display: none;color: #a94442" id="msj_id_status_gestion_laboral"></p>
                        </div>
                        <div class="form-group">
                            <label>Ámbito Gestión/Laboral -Bioética -PROYECTOS o ACTIVIDADES</label>
                             <p>Si contestó "ACTIVO" o "INACTIVO" en la pregunta anterior, usted trabaja o ha trabajado en alguna área relacionada con la bioética, o se desempeña en algún cargo en el que practique sus conocimientos de ésta. Coloque su(s) labore(s) o actividad(es) junto con la(s) institución(es) en la(s) que labora o ha laborado, a continuación (MÁXIMO 3 respuestas):</p>
                            <div class="form-group">
                                <br>
                                <input type="text" class="form-control" name="respuesta1-1" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta2-1" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta3-1" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ámbito Gestión/Laboral -Áreas de la Bioética</label>
                            <p>Si usted contestó la pregunta anterior, seleccione por favor, una o varias de las “Áreas de la Bioética” que se relacionan con las áreas laborales que colocó en la pregunta anterior</p>
                            @foreach($ambitos as $am)

                                @if($am->nombre == 'Otro')
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_laboral_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}

                                </label><input type="text" name="gestion_laboral" autocomplete="off">
                                </div>
                                @else
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_laboral_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}

                                </label>
                                </div>
                                @endif

                            @endforeach

                        </div>
                        <div class="form-group">
                            <label>Ámbito de Formación/Docencia en Bioética - STATUS *</label>

                            <p>STATUS ACTUAL: ACTIVO= si desempeña en cargos de docente o en formación en Bioética. // INACTIVO= Actualmente no se desarrolla en ese ámbito de la Bioética PERO lo ha hecho antes. // INEXISTENTE= NUNCA se ha desempeñado en este ámbito de la Bioética.</p>

                            <div id="status_form_docencia">
                                @foreach($statusL as $sL)
                            <div class="form-group">
                                <input type="radio" name="id_status_form_docencia"  class="id_status_form_docencia" value="{{$sL->id}}" >&nbsp;&nbsp;{{$sL->descripcion}}
                            </div>

                            @endforeach
                            </div>
                            <p style="display: none;color: #a94442" id="msj_id_status_form_docencia"></p>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de Formación/Docencia en Bioética- TEMAS/ ÁREA</label>

                            <p>Si usted contestó "ACTIVO" o "INACTIVO", se desempeña o se desempeñó en la formación o de la docencia sobre algún tema de Bioética, o que pudiera tener relación. Coloque los tema(s) o clase(s) que imparte(ió), o áreas en las que desempeña (ó) e institución(es) en la(s) que realiza(ó) estas actividades, a continuación (Máximo 3 respuestas).</p>
                            <div class="form-group">
                                <br>
                                <input type="text" class="form-control" name="respuesta1-2" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta2-2" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta3-2" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de Formación/Docencia- Área de la Bioética</label>
                            <p>SI usted contestó la pregunta anterior, seleccione por favor, una o varias de las “Áreas de la Bioética” que se relacionan con las actividades en el área de la Formación o de la Docencia en las que desarrolla o ha desarrollado</p>
                            @foreach($ambitos as $am)
                             @if($am->nombre == 'Otro')
                             <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_docencia_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}

                                </label><input type="text" name="formacion_docencia" autocomplete="off">
                                </div>
                                @else
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_docencia_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}

                                </label>
                                </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label>Ámbito de Investigación en Bioética - STATUS * </label>

                            <p>SELECCIONE una de las siguientes con respecto a su actividad en investigación: STATUS ACTUAL: ACTIVO= se desarrolla actualmente como investigador en Bioética. // INACTIVO= cuenta con investigaciones previas, pero no en desarrollo. // INEXISTENTE=no cuenta con investigaciones previas o en curso en el ámbito de la Bioética.</p>
                            <div id="status_invest_bio"></div>
                                   @foreach($statusL as $sL)
                            <div class="form-group">

                                <input type="radio" name="id_status_invest_bio" class="id_status_invest_bio"  value="{{$sL->id}}" >&nbsp;&nbsp;{{$sL->descripcion}}
                            </div>

                            @endforeach

                            <p style="display: none;color: #a94442" id="msj_id_status_invest_bio"></p>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de Investigación en Bioética - LÍNEAS DE INVESTIGACIÓN / TEMAS</label>
                            <p>Si usted contestó "ACTIVO" o "INACTIVO", se desempeña como investigador en algún área de la Bioética, o cuenta con investigaciones anteriores en esta área. Coloque los tema(s) e institución(es) en la(s) que realiza estas actividades, a continuación:</p>
                            <textarea  id="" cols="30" rows="10" name="investigacion_temas" class="form-control" style="max-width: 100%;max-height: 200px" ></textarea>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de Investigación - Área de la Bioética</label>
                            <p>SI usted contestó la pregunta anterior, seleccione por favor, una o varias de las “Áreas de la Bioética” que se relacionan con las actividades en la investigación en las que desarrolla o ha desarrollado:</p>
                            @foreach($ambitos as $am)
                             @if($am->nombre == 'Otro')
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_inves_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}

                                </label>
                                 <input type="text" name="temas_area" autocomplete="off">
                                </div>
                                @else
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_inves_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}

                                </label>
                                </div>
                            @endif
                            @endforeach

                        </div>
                        <div class="form-group">
                            <label>Medios de contacto</label>

                            <p>Esto es para que nos brinde el/los medios de contacto para que le contacten.</p>

                        </div>
                        <div class="form-group">
                            <label>Teléfono *</label>
                            <input type="text" class="form-control id_telefono" name="telefono" id="telefono" onkeyup="msjclear('msj_id_telefono');" autocomplete="off" required="">
                             <p style="display: none;color: #a94442" id="msj_id_telefono"></p>
                        </div>
                        <div class="form-group">
                            <label>Teléfono Secundario</label>
                            <input type="text" class="form-control" name="telefono2" autocomplete="off" placeholder="Eje: +52 55 5627 0210 ext. 7100 o Whatsapp 558005089800">
                        </div>
                        <div class="form-group" id="correo">
                            <label>Correo  *</label>
                            <input type="email" class="form-control id_correo" name="correo" autocomplete="off" onkeyup="msjclear('msj_id_correo');" id="searchMail" oncopy="return false;" onpaste="return false;" oncut="return false;">

                        </div>
                        <p style="display: none;color: #a94442" id="msj_id_correo"></p>
                        <p id="msj_mailT" style="color: #a94442"></p>

                        <div class="form-group">
                            <label>Correo Secundario </label>
                            <input type="email" class="form-control" name="correo2" autocomplete="off">
                        </div>

                        <div class="form-group" id="palabras_claves">
                            <label>Palabras clave</label>
                            <p>A continuación coloque las palabras clave por las cuales a usted le gustaría ser encontrado en los filtros de búsqueda de la página. Estas palabras tienen que tener relación a sus trabajos en el campo laboral, docencia o investigación. Pueden ser palabras compuestas. Ejemplo: "neuroética", "derechos humanos", "pedagogía"</p>
                            <div class="s2-example">
                                <br>
                                <input type="text" name="palabras_claves"  class="form-control id_palabras_claves" onkeyup="msjclear('msj_id_palabras_claves');" required="">
                        <p style="display: none;color: #a94442" id="msj_id_palabras_claves"></p>

                            </div>
                        </div>

                        <div class="form-group">
                            <strong style="text-align: justify;padding-left: 10px;">AVISO DE PRIVACIDAD:</strong>
                            <br>
                            <p style="font-size: 12px;text-align: justify;padding-left: 10px;padding-right: 10px;font-weight: 700;line-height: initial;">
                                Manifiesto mi consentimiento bajo información para que sean utilizados mis datos personales y/o sensibles, por la Facultad de Bióetica de la Universidad Anáhuac, proporcionados expresa y libremente, para los efectos legales correspondientes, de conformidad con la Ley de Protección de Datos en Posesión de Particulares, bajo principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad, previsto en la Ley. Con la finalidad de lograr la visibilidad y vinculación buscada, los datos que proporcione, serán visibles para cualquier persona en internet, es por esto que me hago responsable de la veracidad de estos y proporciono únicamente datos que desee exponer en la red: <br><br>
                                Artículo 16.- El aviso de privacidad deberá contener, al menos, la siguiente información:
                                I.- La identidad y domicilio del responsable que los recaba;
                                II.- Las finalidades del tratamiento de datos; <br>
                                II.- Las opciones y medios que el responsable ofrezca a los titulares para limpiar el uso o divulgación de datos; <br>
                                IV.- Los medios para ejercer los derechos de acceso, rectificación u oposición, de conformodidad con lo dispuesto en esta ley; <br>
                                V.- En su caso, las transferencias de datos que se efectúen, y. <br>
                                VI.- El procedimiento y medio por el cual el responsable comunicará a los titulares de cambios al aviso de privacidad con lo previsto en esta Ley. En el caso de datos personales sensibles, el aviso de privacidad deberá señalar expresamente que se trata de este tipo de datos.
                            </p>
                            <p align="right" ><b id="term_msj" style="display: none;color: #a94442;margin-right: 10px;font-weight: 100"></b>  Aceptar <input type="checkbox" name="terminos" class="terminos"></p>
                        </div>
                        <div class="col-lg-3"></div>
                        <div class="form-group col-lg-6 col-lg-offset-0 col-md-4 col-md-offset-4 col-sm-5 col-sm-offset-4 col-xs-12" style="padding: 0">
                            <table>
                                <tr>
                                    <td><center><i class="fa fa-refresh btn btn-xs" data-toggle="tooltip" title="Otro" id="loadCaptcha"></i></center></td>
                                </tr>
                                <tr>
                                    <td><center><div class="imgcaptcha"></div></center></td>
                                </tr>
                            </table>
                            <input type="hidden" id="captchaVal">
                            <input type="hidden" id="recaptchaRes" name="recaptcha">
                            <input type="text" name="captcha" id="valcaptcha" maxlength="10"  style="text-align: center; margin-top: 10px;width: 80%;height: 20px;padding-top: 0;" >
                            <a class="btn btn-xs " id="sendCaptcha" style="top: -1px;position: relative;">validar</a><br>
                            <b id="msj_captcha" style="display: none;color: #a94442;margin-right: 10px;font-weight: 100"></b>
                        </div>
                        <div class="col-md-12" id="guardando" style="display: none;"><center><label> Guardando Datos..</label><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></center></div>
                        <div class="form-group" id="guardar">
                            <button type="submit" class="btn btn-block btn-success" id="sendbutton" style="padding: 19px 0px;">ENVIAR&nbsp;&nbsp;<i class="fa fa-send"></i></button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
      <!-- Modal agregar formación-->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center">Nueva Formación <i class="fa fa-graduation-cap"></i></h4>
            </div>
            <form id="form_licen" class="form_new" action="{{url('/newFormacion')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
                <div class="modal-body">
                        <div class="form-group">
                            <label>Identifique el área a la que corresponde su licenciatura</label>
                            <br>
                               {{--  @foreach($otros as $alic)
                                <div class="form-group">
                                    <input type="hidden" name="id_tooltip_carrera" value="{{$alic->id_tooltip_carrera}}">
                                    <input type="hidden" name="color" value="{{$alic->color}}">
                                    <input type="radio" name="area"  value="{{$alic->areasLic}}" required="">&nbsp;&nbsp;{{$alic->title}}
                                </div>
                                @endforeach --}}
                                 @foreach($areasLic as $alic)
                                <div class="form-group">
                                    <input type="radio" name="area" class="id_area_LIC" value="{{$alic->id}}" required="" >&nbsp;&nbsp;{{$alic->nombreArea}}
                                </div>
                                @endforeach
                        </div>

                  <input type="text" class="form-control" id="norepeatformacion" name="title" placeholder="Ingresa Nombre de Formación" autocomplete="off" required="">
                  <p style="color: #a94442" id="msj_form"></p>
                </div>
                <div class="modal-footer">
                  <div class="col-md-12" id="saveformacion" style="display: none;"><center><label> Guardando Datos..</label><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></center></div>
                        <div class="form-group" id="guardarformacion">
                            <button type="submit" class="btn btn-block btn-success" id="sendform" style="padding: 19px 0px;">Guardar</button>
                        </div>
                </div>
            </form>
          </div>
        </div>
      </div>
      <!--  end Modal -->
</div>
</body>
@endsection
@section('js')
<script src="{{ asset('/js/mainUnete.js')}}"></script>
@endsection
