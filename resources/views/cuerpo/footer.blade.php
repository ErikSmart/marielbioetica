<footer class="container-fluid pie">
  <div class="row">
    <div class="col-md-12 hidden-xs hidden-sm footinitial">
      <div class="col-lg-6 col-md-7 col-xs-12">
        <table style="position: relative;top: 18px;">
          <tr>
            <th class="barravertical">
              <a href="{{url('/quienes-somos')}}"  title="Quiénes somos los de área de bioética" class="enlacespie" style=" padding-right: 5px;padding-left: 5px">QUIÉNES SOMOS</a>
            </th>
            <th class="barravertical">
              <a href="{{url('/areas-de-bioetica')}}" class="enlacespie"  title="Áreas disponibles del la bioética" style=" padding-right: 5px;padding-left: 5px">ÁREAS DE BIOÉTICA</a>
            </th>
            <th class="barravertical">
              <a href="{{url('/mapa')}}" class="enlacespie"  title="Encuentra un profecional en bioética" style=" padding-right: 5px;padding-left: 5px">MAPA</a>
            </th>
            <th class="barravertical">
              <a href="{{url('/unete')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">ÚNETE</a>
            </th>
            <th class="barravertical">
              @if (Auth::guest())
              <a href="{{ url('/login') }}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">LOGIN</a>
              @else
              @if(Auth::user()->perfil_id == 1)
              <a href="{{url('/home')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
              @else
              <a href="{{url('/misdatos')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
              @endif
              @endif

            </th>
          </tr>
        </table>

      </div>
      <div class="col-lg-6 col-md-5">
        <table class="contactolog">
          <tr>
            <td class="barravertical"><a href="{{url('/contactanos')}}" class="enlacespie espaciocontacto" title="Contacto" style=" padding-right: 5px;padding-left: 5px">CONTACTÁNOS</a>
            </td>&nbsp;&nbsp;
            
            <td>
            <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//suas-emc2-b.anahuac.mx/">
              <img src="{{asset('/img/iconos/facebook-red-vinculacion-anahuac.png')}}"  class="img-responsive red-tooltip" alt=""width="32" height="32" data-toggle="tooltip" data-placement="top" title="Compartir la red en Facebook">
            </a>
             </td>
             <td>
          <a href="https://twitter.com/home?status=http%3A//suas-emc2-b.anahuac.mx/">
            <img src="{{asset('/img/iconos/twitter-red-vinculacion-anahuac.png')}}"  class="img-responsive" alt=""width="32" height="32" data-toggle="tooltip" data-placement="top" title="Compartir la red en Twitter">
          </a>
              </td>
              <td>
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//suas-emc2-b.anahuac.mx/&title=&summary=&source=">
              <img src="{{asset('/img/iconos/linkedin-red-vinculacion-anahuac.png')}}"  class="img-responsive" alt=""width="32" height="32" data-toggle="tooltip" data-placement="top" title="Compartir la red en Linkedin">
              </a>
              </td>
              <td>
              <img src="{{asset('/img/logo/logo-pie-anahuac.png')}}" alt="anahuac" class="centrarimagenpie">
            </td>
          </tr>
        </table>

      </div>
    </div>

    <!-- footer para telefono -->
    <div class="col-md-12 hidden-lg hidden-md">
      <center>
      <a href="{{url('/quienes-somos')}}"  title="Quiénes somos los de área de bioética" class="enlacespie" >QUIÉNES SOMOS</a>
      <br>
      <a href="{{url('/areas-de-bioetica')}}" class="enlacespie"  title="Áreas disponibles del la bioética" >ÁREAS DE BIOÉTICA</a>
      <br>
      <a href="{{url('/mapa')}}" class="enlacespie"  title="Encuentra un profecional en bioética" >MAPA</a>
      <br>
      @if (Auth::guest())
      <a href="{{ url('/login') }}" class="enlacespie"  title="Formulario registrate en bioética" >LOGIN</a>
      @else
      @if(Auth::user()->perfil_id == 1)
      <a href="{{url('/home')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
      @else
      <a href="{{url('/misdatos')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
      @endif
      @endif
      <br>
      <a href="{{url('/contactanos')}}" class="enlacespie" >CONTACTÁNOS</a>
      <img src="{{asset('/img/logo/logo-pie-anahuac.png')}}" alt="anahuac" width="52" height="52" class="centrarimagenpie">
      </center>
      <center>
      <table class="hidden-lg hidden-md">
        <tr>
          <td>
            <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//suas-emc2-b.anahuac.mx/">
            <img src="{{asset('/img/iconos/facebook-red-vinculacion-anahuac.png')}}"  class="img-responsive" alt=""width="32" height="32">
          </a>
          </td>
          <td>
            <a href="https://twitter.com/home?status=http%3A//suas-emc2-b.anahuac.mx/">
          <img src="{{asset('/img/iconos/twitter-red-vinculacion-anahuac.png')}}"  class="img-responsive" alt=""width="32" height="32">
        </a>
          </td>
          <td>
            <a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//suas-emc2-b.anahuac.mx/&title=&summary=&source=">
            <img src="{{asset('/img/iconos/linkedin-red-vinculacion-anahuac.png')}}"  class="img-responsive" alt=""width="32" height="32">
            </a>
          </td>
        </tr>
      </table>
      </center>
    </div>
  </div>
</footer>
