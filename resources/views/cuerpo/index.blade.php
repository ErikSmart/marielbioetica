<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Red de Vinculación Facultad de Bioética</title>
    <link rel="shortcut icon" href="{{ asset('/img/logo.gif')}}" />
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/css/inicio.css')}}">
        <!-- Script Analytics Smartlook-->
            {{-- <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', 'ec38c4b554fb937e48552f3cc45c0deb7f641e41');
            </script> --}}
  </head>
  <body class="portada">
    <div class="container-fluid franja-negra"></div>
    <div class="container inicio-bio">
      <div class="row">
        <div class="col-sm-12">
          <div class="centrar">
            <center>
            <table>
              <tr>
                <th colspan="2">
                  <center>
                  <img src="{{ asset('/img/logo/logo-portada-bietica.png')}}" alt="Red de vinculación de bioética" class="img-responsive">
                  </center>
                  <br>
                  <br>
                </th>
              </tr>
              <tr>
                <td><a href="{{url('/unete')}}" class="boton-portada1">ÚNETE</a></td>
                <td><a href="{{url('/mapa')}}" class="boton-portada">MAPA</a></td>
              </tr>
            </table>
            </center>
          </div>
        </div>
      </div>
    </div>
    <!-- footer -->
    @include('cuerpo.footer')
    <!-- end footer -->
  </body>
  <!-- jQuery 2.1.4 EACS-->
  <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <!-- Bootstrap 3.3.2 JS redsocialcurunam-->
  <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
</body>
</html>
