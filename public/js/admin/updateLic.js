
$(document).ready(function() {
      
      callLicenciaturas()
} );

  function callLicenciaturas(){
 var table = $('#Lic').DataTable();
      table.destroy();
       var table = $('#Lic').DataTable( {
          "scrollX": true,
    "order": [],
        "language": {
 
    "sProcessing":     "Procesando...",
 
    "sLengthMenu":     "Mostrar _MENU_ registros",
 
    "sZeroRecords":    "No se encontraron resultados",
 
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
 
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
 
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
 
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
 
    "sInfoPostFix":    "",
 
    "sSearch":         "Buscar:",
 
    "sUrl":            "",
 
    "sInfoThousands":  ",",
 
    "sLoadingRecords": "Cargando...",
 
    "oPaginate": {
 
        "sFirst":    "Primero",
 
        "sLast":     "Último",
 
        "sNext":     "Siguiente",
 
        "sPrevious": "Anterior"
 
    },
 
    "oAria": {
 
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
 
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
 
    }
 
},

          "ajax": "/dataLic",
         
            columns: [
                { data: "title" },
                { data: "areasLic" },
                { data: "color" },


                {
                    "data": null,
                    "defaultContent": "<button class='btn btn-info editLic'><i class='fa fa-pencil'></i></button>"
                },
                {
                    "data": null,
                    "defaultContent": "<button class='btn btn-danger deleteLic'><i class='fa fa-trash'></i></button>"
                },
            ]
     
    });
      
    $('#Lic tbody').on( 'click', '.editLic', function () {
        var data = table.row( $(this).parents('tr') ).data();
        $.get('/viewEditLic/'+data['id']+'/', function(response) {
          
          $('#ModalLIC').modal('show'); 
          $('#contenido').html(response);//imprimir contenido html


        });
    } );
    $('#Lic tbody').on( 'click', '.deleteLic', function () {

        var data = table.row( $(this).parents('tr') ).data(); 
         var mapView = data['mapView'];
    if (mapView == 0) {
 $.confirm({
                    title:'Confirmación de Acción',
                    icon: 'fa fa-warning text-orange',
                    content:'<center><h3>Seguró que quieres Eliminar la Licenciatura</h3> <h4>"<b>'+data['title']+'</>"</h4></center>',
                    confirmButton: 'Continuar',
                    confirmButtonClass: 'btn-success' ,
                    cancelButtonClass:'btn-danger',
                    cancelButton: 'Cancelar',
                    backgroundDismiss: true,
                    cancel: function(){
                     
                  },
                   confirm: function()
                   {   
                
                    $.get('/trashLic/'+data['id'], function(response) {

                               $.confirm({
                                  title: 'Desactivada',
                                  content:'<center><h2>Formación Eliminada</h2></center>',
                                  confirmButton: 'Continuar',
                                  confirmButtonClass: 'btn-success',
                                  cancelButton: false,
                                  confirm: function(response)
                                  {
                                     $('#Lic').empty();
                                      callLicenciaturas()

                                  },
                                  error:function(response){

                                  }
                              });
                              
                          }); 

            
                    }
                });
    }else{
      $.alert({
        title:'Información',
        content:'<h2><center>Esta Licenciatura no puede eliminarse solo puede editarse</center></h2>',
        confirmButton: 'Continuar',
        confirmButtonClass: 'btn-info' ,
        });}
         
      });

  }


     $(document).on("submit",".updateLIC",function(e){
//funcion para atrapar los formularios y enviar los datos

            e.preventDefault();
            var formu=$(this);
            var quien=$(this).attr("id");
            var token = $("#token").val();
            if(quien=="form_updateLic"){ var miurl="/updateLic"; }

            var title = $('.titleLic').val();
            var color = $('.color').val();
            if(!title){
                        $.dialog('<h3>Debes ingresar un titulo</h3>');
                        return false;
                    }
            if(!color){
                      $.dialog('<h3>Debes ingresar un color</h3>');
                      return false;
              }
            $.ajax({
                  url: miurl,
                  type: 'post',
                  headers:{'X-CSRF-TOKEN':token},
                  data : formu.serialize(),
                  beforeSend:function(){
                      $('#guardando').show();
                      $('#guardar').hide();
                  },
                  success:function(response){
                    $('#guardar').show();
                    $('#guardando').hide();
                    $('#Lic').empty();
                    $('#ModalLIC').modal('hide'); 

                    callLicenciaturas()
                    $.dialog({
                        title: 'Correcto',
                        icon:'fa fa-thumbs-o-up text-success',
                        content: '<center><h2><b>Cambios echos con exito</b></h2></center>',
                    });
                  },
                  error:function(){
                    $.alert('<center><b>¡Error al guardar datos!</b></center>');
                    return false;
                  }
                });
            })
