$(document).ready(function() {


  loadCaptcha();
  /*cargar img captcha para el formulario*/
  $('#loadCaptcha').click(function(event) {
    $('.imgcaptcha').empty();
    $('#valcaptcha').val('');
    $('#msj_captcha').empty();
    $('#valcaptcha').removeClass('inputCapcha');
    $('#recaptchaRes').val('false');
    $('#valcaptcha').removeClass('inputCapcha').attr('disabled', false);

    loadCaptcha();
  });
  $('#valcaptcha').keyup(function(event) {

    $('#msj_captcha').empty();

  });
});

// a function called checkCaptcha instead of a listener of sendCaptcha is 
// more easy
function checkCaptcha() {
  // the user never fill a captchar with uppercase and 
  // lowercase letter, is better use .toUpperCase for it
  valorInput = $('#valcaptcha').val().toUpperCase();
  valorCapt  = $('#captchaVal').val().toUpperCase();
  if (valorInput == valorCapt) {
    $('#valcaptcha').addClass('inputCapcha').attr('disabled', true);
    $('#recaptchaRes').val('true');
    //alert(valorInput+' '+valorImg);
    // $('#sendbutton').attr('disabled', false);
  }else{
    $('#valcaptcha').removeClass('inputCapcha').attr('disabled', false);
    $('#recaptchaRes').val('false');

    // $('#sendbutton').attr('disabled', true);
    $('#msj_captcha').empty().show().append('El valor no coincide');
  }

}

function loadCaptcha(){
  $.ajax({
    cache: false,
    url: '/captcha',
    type: 'GET',
    beforeSend:function(){
      $('.imgcaptcha').empty().append('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>');
    },
    success:function(response){
      img = response.data.uri;
      $('.imgcaptcha').empty().append('<img src="data:image/png;'+img+'" class="img-responsive"/>');

      //alert(JSON.stringify(response.codigo));
      valorImg = response.codigo;
      $('#captchaVal').val('');
      $('#captchaVal').val(valorImg);

    }
  });
}
