$(function () {
  $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
  });
});


/*se abtienen los valor del onclick*/        
function insertCookie(name, ruta){
           
    localStorage.setItem( name , ruta );
}    


function myFunction() 
{
  $('#loading').show();
  var clave = $('#myInput').val();
  var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
      
  $.get('/searchClave/'+clave+'', function(response) {
      $("#myUL").empty();
      $('#loading').hide();

    for (var i = 0; i < response.length; i++) {

      ruta = Base64.encode("/ver/"+response[i].nombreCompleto+"/"+response[i].id);
      ruta_ir = "/ver/"+response[i].nombreCompleto+"/"+response[i].id;
      name = response[i].nombreCompleto;
      status = response[i].status;
   
      if ( status == 'Activo') {
        datas='<li class="list-group-item"><a onClick="insertCookie(\''+name+'\' , \' '+ruta+' \');" href="'+ruta_ir+'" href="" style="color: #000"> '+response[i].nombreCompleto+'</a></li>';
    $("#myUL").append(datas);

      }
 
    }
  });

  if ($('#myInput').val() == '') {
      $("#myUL").empty();
      $('#loading').hide();
  }

  var input, filter, ul, li, a, i;
  input = document.getElementById('myInput');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('li');
  // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
      if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = " ";
      } else {
        li[i].style.display = "none";
      }
    }
    
}

$(document).on("submit",".form_change",function(e){
//funcion para atrapar los formularios y enviar los datos

e.preventDefault();
         
var formu=$(this);
var quien=$(this).attr("id");//quien es el formulario de donde provienen los datos
//se declara el id del formulario con su ruta 

if(quien=="password_edit"){ var miurl="/cambiar_password";  var pass1 = $('#newpassword').val(); var pass2 = $('#password_confirmation').val();}
  
  if (pass1 != '' && pass2 != '') {
      $.ajax({
          type: "POST",
          url : miurl,
       
          data : formu.serialize(),
          success : function(response){
               if (response == 1) {
                  $.confirm({
                      icon:'glyphicon glyphicon-ok text-green',
                      title: 'Datos Actualizados',
                      content: '<center><h3>La actualizaci&oacute;n de tus datos se realizo correctamente</h3></center>',
                      confirmButtonClass: 'btn-success',
                      confirmButton: 'Confirmar',
                      cancelButton: false,
                      confirm: function () {
                        location.reload(true);
                       } 
                    });
               }
            }, //si ha ocurrido un error
          error: function(response){  
                $.confirm({
                  icon: 'fa fa-warning',title:'Hoops!',
                  content:msj,
                  confirmButton: 'Confirmar',
                  cancelButton: false,
                  animation: 'zoom', confirmButtonClass: 'btn-info',
                });
          }
    });
  }else{
        $("#subContra").attr('disabled', true);

    $.dialog({title:false,content:'<cener><h2>Completar campos</h2></center>'});
  }
  
})

$(document).ready(function($){
    /*captura el tamaño del div y emparejalo con el de contacto*/
    var table_alto = $("#contenlist").height();
    //alert(parseFloat(table_alto));
    $(".contentinfo").css({'height':table_alto});
    $('[data-toggle="tooltip"]').tooltip();

    /*slimScroll de resultados en la vista mapa*/
    $('#datosSearch').slimScroll({
         height: '400px'
     });
/*    $('#myUL').slimScroll({
         height: '400px'
     });*/
  
    $(".center").slick();


  $('#newpassword').keyup(function(event) {
      var pass1 =$('#newpassword').val();

    if(pass1.length < 6) {  
        $("#msj_pass1").empty().append("El m&iacute;nimo es 6 car&aacute;cteres");  
        $("#subContra").attr('disabled', true);
        
      } else{
        $("#msj_pass1").empty();  
        $("#subContra").attr('disabled', false);

      }
  });
  $('#password_confirmation').keyup(function(event) {
      var pass1 =$('#newpassword').val();
      var pass2 =$('#password_confirmation').val();

      if(pass2.length < 6) {  
        $("#msj_pass").empty().append("El m&iacute;nimo es 6 car&aacute;cteres");  
          
      } else{
        $("#msj_pass").empty();  

      }

      if (pass1 == pass2) {
        $("#subContra").attr('disabled', false);
      $("#msj_pass").empty();

      }else{
        $("#subContra").attr('disabled', true);
      $("#msj_pass").empty().append('La contraseña no coincide');
      }
    });

    /*mostrar listado o grafica*/
    $(".ShowMapa").click(function(event) {
      $("#listadoColor").hide();
      $("#listadoResult").hide();
      $("#graficaResult").show();
      $("#capaRespuesta").removeClass('capaRespuesta').addClass('capaRespuestaMapa')
      $(".dondeList").removeClass('dondeList').addClass('donde')

      $("#mostrar").hide();
      Listopacity("path35748-6");
      Listopacity("path35752-6");
      Listopacity("path35756-6");
      Listopacity("path35760-8");
      mapaBlod("path35776-0");
      mapaBlod("path35780-3");
      mapaBlod("path35784-5");
      mapaBlod("path35788-2");
      mapaBlod("path35792-8");
      mapaBlod("path35796-7");

    });
    /*al dar click en el icono de lista de mapa mostar*/
     $(".ShowListado").click(function(event) {
      $("#listadoColor").show();
      $("#listadoResult").show();
      $("#ShowListado").removeClass('capaRespuestaMapa').addClass('capaRespuesta')
      $(".donde").removeClass('donde').addClass('dondeList')

      $("#graficaResult").hide();
      $("#mostrar").show();
      /*cambiar color del mapa visualizacion*/
   
      Mapaopacity("path35776-0");
      Mapaopacity("path35780-3");
      Mapaopacity("path35784-5");
      Mapaopacity("path35788-2");
      Mapaopacity("path35792-8");
      Mapaopacity("path35796-7");

      listBlod("path35748-6");
      listBlod("path35752-6");
      listBlod("path35756-6");
      listBlod("path35760-8");


    });
  
});
function Mapaopacity(id){
    $("#"+id).css({
        'fill-opacity': '0.3',
        'stroke-opacity':'0.3'
    });

}
function listBlod(id){
  $("#"+id).css({
        'stroke-width': '3px',
  });
}
function Listopacity(id){
    $("#"+id).css({
        'stroke-width': '1px',
    });
}
function mapaBlod(id){
  $("#"+id).css({
        'fill-opacity': '1',
        'stroke-opacity':'1'
        
  });
}
