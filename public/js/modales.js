function fichaUsuario(id){
 
    $.get('/sesionA/'+id, function(response) {
       if (response == true) { 
        /*ocultar boton de aceptar usuario para crear sesion*/
        $('#aceptarU').hide();  
         
       }else{
        $('#aceptarU').show();  
       }
    });
     $.get('/fichaUsuario/'+id , function(response) {
    
       $('#ModalDetalles').modal('show'); 
       $('#datosUsuario').html(response);//imprimir contenido html

     }); 
}
function validarUser(){
  var id = $('#idUser').val();

  $.confirm({
          title: false,
          content:'<center><h2>Quieres aceptar este Usuario</h2><strong>al aceptar este usuario podran ser visto sus datos en en el mapa</strong></center>' ,
          animation: 'zoom',
          columnClass: 'col-md-4 col-md-offset-4',
          closeAnimation: 'scale',
          confirmButton: 'Aceptar Usuario',
          confirmButtonClass: 'btn-success',
          cancelButtonClass: 'btn-info',
          cancelButton: 'salir',
          confirm: function()
          {
           $.get('/validateUser/'+id, function(response) {
              $.confirm({
                title:'Confirmación',
                icon:'fa fa-check text-green',
                content:'<center><h2>Usuario Aceptado</h2></center>',
                confirmButton: 'Ok',
                confirmButtonClass: 'btn-info',
                cancelButton: false,
                confirm: function()      
                {$('#ModalDetalles').modal('hide'); location.reload(true);}
              });
            });
          },
          cancel:function(){

          }
         });
}

function desactivar(id){

 var url="/desactivarsesion";
  var msj ="<u>Desactivar</u>";


        $.confirm({
          title:'Confirmación de Acción',
          icon: 'fa fa-warning text-orange',
          content:'<center><h2>Seguró que quieres '+msj+' esta sesión</h2><strong>al confirmar el usuario no podra logearse</strong></center>',
          confirmButton: 'Continuar',
          confirmButtonClass: 'btn-success',
          cancelButtonClass: 'btn-danger',
          cancelButton: 'Cancelar',
          cancel: function(){
           
        },
         confirm: function()
         {   

          $.get(url+'/'+id, function(response) {

                     $.confirm({
                        title: 'Desactivada',
                        content:'<center><h2>'+response+'</h2></center>',
                        confirmButton: 'Continuar',
                        confirmButtonClass: 'btn-success',
                        cancelButton: false,
                        confirm: function()
                        {
                           location.reload(true);

                        }
                    });
                    
                }); 

  
          }
      });
}
function activar(id){

 var url="/activarsesion";
  var msj ="<u>Activar</u>";


        $.confirm({
          title:'Confirmación de Acción',
          icon: 'fa fa-warning text-orange',
          content:'<center><h2>Seguró que quieres '+msj+' esta sesión</h2><strong>al confirmar el usuario ya puede logearse</strong></center>',
          confirmButton: 'Continuar',
          confirmButtonClass: 'btn-success',
          cancelButtonClass: 'btn-danger',
          cancelButton: 'Cancelar',
          cancel: function(){
           
        },
         confirm: function()
         {   

          $.get(url+'/'+id, function(response) {

                     $.confirm({
                        title: 'Desactivada',
                        content:'<center><h2>'+response+'</h2></center>',
                        confirmButton: 'Continuar',
                        confirmButtonClass: 'btn-success',
                        cancelButton: false,
                        confirm: function()
                        {
                           location.reload(true);

                        }
                    });
                    
                }); 

  
          }
      });
}
function configTimes(){

     $.get('/configTimes', function(response) {
    
       $('#ModalTimes').modal('show'); 
       $('#viewTimes').html(response);//imprimir contenido html

     }); 
}