
/*optenemos la url al dar click sobre la seccion del mapa*/
 mapClick=(idfilter)=>{
  $(".donde").removeClass('donde').addClass('dondeList');
      $("#preloadDiv").show();
      var token = $("#tokenCanvas").val();
      $('#myUL').hide();
      $("#resultados").show();
      $("#listadoResult").hide();
      $("#graficaResult").show();
       $("#listadoColor").show();

      $.ajax({
          url: '/buscando',
          type: 'GET',
          dataType: 'JSON',
          data: {where:idfilter},
          headers:{'X-CSRF-TOKEN': token},
          beforeSend: function(){
            efecto();
            $(".result").empty();
            $("#capaRespuesta").addClass('capaRespuesta');
            $("#showButton").hide();
            $("#mostrar").show();
          },
          success: function (response) {
            $('.nohaydatos').empty();
            ocultaSelect();
            $("#preloadDiv").hide();
            $('#graficaResult').hide()
            $('#listadoResult').show()

             $("#listadoColor").show();

             /*cambiar color del mapa visualizacion*/

              Mapaopacity("path35776-0");
              Mapaopacity("path35780-3");
              Mapaopacity("path35784-5");
              Mapaopacity("path35788-2");
              Mapaopacity("path35792-8");
              Mapaopacity("path35796-7");

              listBlod("path35748-6");
              listBlod("path35752-6");
              listBlod("path35756-6");
              listBlod("path35760-8");
              /*end cambiar color de mapa*/



            if(!$.isArray(response) || !response.length){
                $('.nohaydatos').append('<div class="col-md-12" style=" border-bottom: 3px solid #ccc; padding-top: 15px; padding-bottom: 15px"><center>No se encontraron resultados</center></div>');
                $('#myInput').val('');
              }
            var Base64={
              _keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
              encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}
              }
             for (i = 0; i < response.data.length; i++){
                //alert(JSON.stringify(response));
                nameespacio = response.data[i].nombreCompleto;
                name = nameespacio.replace("%20"," ");
                name_ruta = response.data[i].url+"/"+response.where;
                ruta_ir = name_ruta;
                ruta = Base64.encode(name_ruta);
                /*mostrar los valores donde la fecha deleted sea NULL*/

                $('.result').append('<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style=" border-bottom: 3px solid #ccc; padding-top: 15px; padding-bottom: 15px"><a  onClick="insertCookie(\''+name+'\' , \' '+ruta+' \');" href="'+ruta_ir+'"><div class="media"><div class="col-lg-4 col-md-5 col-sm-4 col-xs-12" style="color:#fff;margin-right: 6px;position: relative;height: 95px;background-color: '+response.data[i].color+' ">'+response.data[i].nombreCompleto+'</div><div class="media-body" style="font-size: 14px;padding-top:10px;"><h4 class="media-heading" style="font-size: 12px;"><i class="fa fa-circle" style="color: '+response.data[i].color+' "></i> '+response.data[i].acronimo+'</h4> <p style="color:#000; padding-left:15px;font-size: 13px;line-height: inherit; margin-bottom:0;">'+response.data[i].rgestion.substr(0,80)+'... </p></div></div></a></div> ');
                $('.nohaydatos').empty();




                $('#myInput').val('');

              }


          }
      });

    }





$(document).ready(function() {
    ver();
	$("#mostrar").click(function(event) {
      $("#contentSection").show();
      $("#resultados").hide();

      $('#ocultar').css('display','block');
      $(".result").empty();
      $('.nohaydatos').empty();
      $('#myUL').show();
      verSelect();
      $("#myUL").empty();

      $(".eliminarCookies").empty();
           $('.eliminarCookies').show();
      /*cargar cookies una vez pulsado mostrar*/
      LoadCookies();

	});

$('#myInput').keyup(function(event) {
  input = $('#myInput').val();
      if (input == '') {

          $('#myUL').show();
          $('.eliminarCookies').empty();

          $('.eliminarCookies').show();
        LoadCookies();


      }else{


        $('#myUL').show();

      }

});

});

/*funcion encargada de mostar loagin mientras los css y js cargan */
function efecto(){
	var alto=$(window).height();
	$("#preloadImage").css({"margin-top":(alto/0)-30+"px"});


}

/*caragador de entradad gif*/
function ver(){
	$(window).load(function() {
    // setTimeout only work with functions as controllers
    // please ever use () => or () => {} this is a arrow
    // function
	  setTimeout(() => showConent(),9000);
	});
}

function showConent(){
    $("#preloadDiv").fadeOut();

    // setTimeout only work with functions as controllers
    // please ever use () => or () => {} this is a arrow
    // function
    setTimeout(() => $("#contentSection").fadeIn(), 2000);
    $("#contentSection").show('2000');
}
function ocultaSelect(){
	$("#dataTogle").removeClass('collapsed').attr('aria-expanded',false);
	$("#collapse1").removeClass('panel-collapse collapse in').addClass('panel-collapse collapse').attr('aria-expanded',false);

	$("#dataTogle2").removeClass('collapsed').attr('aria-expanded',false);
	$("#collapse2").removeClass('panel-collapse collapse in').addClass('panel-collapse collapse').attr('aria-expanded',false);

	$("#dataTogle3").removeClass('collapsed').attr('aria-expanded',false);
	$("#collapse3").removeClass('panel-collapse collapse in').addClass('panel-collapse collapse').attr('aria-expanded',false);
}
function verSelect(){
	$("#dataTogle").addClass('collapsed').attr('aria-expanded',true);
	$("#collapse1").addClass('panel-collapse collapse in').removeClass('panel-collapse collapse').attr('aria-expanded',true);

	$("#dataTogle2").addClass('collapsed').attr('aria-expanded',true);
	$("#collapse2").addClass('panel-collapse collapse in').removeClass('panel-collapse collapse').attr('aria-expanded',true);

	$("#dataTogle3").addClass('collapsed').attr('aria-expanded',true);
	$("#collapse3").addClass('panel-collapse collapse in').removeClass('panel-collapse collapse').attr('aria-expanded',true);
}
