//Para que funcione el menu responsivo
jQuery(document).ready(function () {


    jQuery('ul.nav > li > ul').each(function() {
        jQuery(this).addClass('dropdown-menu');
    });
	jQuery('ul.nav > li.has-child > a').each(function() {
        jQuery(this).addClass('dropdown-toggle');
    });
	 jQuery("ul.nav > li.has-child > a.dropdown-toggle").attr("data-toggle", "dropdown");
	function bindNavbar() {

		if (jQuery(window).width() > 768) {
			jQuery('.has-child').on('mouseover', function(){
				jQuery('.dropdown-toggle', this).next('.dropdown-menu').show();
			}).on('mouseout', function(){
				jQuery('.dropdown-toggle', this).next('.dropdown-menu').hide();
			});

			jQuery('.dropdown-toggle').click(function() {
				if (jQuery(this).next('.dropdown-menu').is(':visible')) {
					window.location = jQuery(this).attr('href');
				}
			});
		}
		else {
			jQuery('.has-child').off('mouseover').off('mouseout');
		}
	}

	jQuery(window).resize(function() {
		bindNavbar();
	});

	bindNavbar();

    

});
