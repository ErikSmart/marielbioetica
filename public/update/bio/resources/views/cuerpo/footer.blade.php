<link rel="stylesheet" href="{{asset('/css/piestilo.css')}}">
<footer class="container-fluid pie">
  <div class="row">
    <div class="col-md-12 hidden-xs hidden-sm footinitial">
      <div class="col-lg-6 col-md-7 col-xs-12">
        <table style="position: relative;top: 18px;">
          <tr>
            <th class="barravertical">
              <a href="{{url('/quienes-somos')}}"  title="Quiénes somos los de área de bioética" class="enlacespie" style=" padding-right: 5px;padding-left: 5px">QUIÉNES SOMOS</a>
            </th>
            <th class="barravertical">
              <a href="{{url('/areas-de-bioetica')}}" class="enlacespie"  title="Áreas disponibles del la bioética" style=" padding-right: 5px;padding-left: 5px">ÁREAS DE BIOÉTICA</a>
            </th>
            <th class="barravertical">
              <a href="{{url('/mapa')}}" class="enlacespie"  title="Encuentra un profecional en bioética" style=" padding-right: 5px;padding-left: 5px">MAPA</a>
            </th>
            <th class="barravertical">
              <a href="{{url('/unete')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">ÚNETE</a>
            </th>
            <th class="barravertical">
              @if (Auth::guest())
              <a href="{{ url('/login') }}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">LOGIN</a>
              @else
              @if(Auth::user()->perfil_id == 1)
              <a href="{{url('/home')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
              @else
              <a href="{{url('/misdatos')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
              @endif
              @endif

            </th>
          </tr>
        </table>

      </div>
      <div class="col-lg-6 col-md-5">
        <table class="contactolog">
          <tr>
            <td class="barravertical"><a href="{{url('/contactanos')}}" class="enlacespie espaciocontacto" title="Contacto" style=" padding-right: 5px;padding-left: 5px">CONTACTÁNOS</a></td>
            <td><img src="{{asset('/img/logo/logo-pie-anahuac.png')}}" alt="anahuac" class="centrarimagenpie"></td>
          </tr>
        </table>
      </div>
    </div>

    <!-- footer para telefono -->
    <div class="col-md-12 hidden-lg hidden-md">
      <center>
      <a href="{{url('/quienes-somos')}}"  title="Quiénes somos los de área de bioética" class="enlacespie" >QUIÉNES SOMOS</a>
      <br>
      <a href="{{url('/areas-de-bioetica')}}" class="enlacespie"  title="Áreas disponibles del la bioética" >ÁREAS DE BIOÉTICA</a>
      <br>
      <a href="{{url('/mapa')}}" class="enlacespie"  title="Encuentra un profecional en bioética" >MAPA</a>
      <br>
      @if (Auth::guest())
      <a href="{{ url('/login') }}" class="enlacespie"  title="Formulario registrate en bioética" >LOGIN</a>
      @else
      @if(Auth::user()->perfil_id == 1)
      <a href="{{url('/home')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
      @else
      <a href="{{url('/misdatos')}}" class="enlacespie"  title="Formulario registrate en bioética" style=" padding-right: 5px;padding-left: 5px">{{ Auth::user()->name }}</a>
      @endif
      @endif
      <br>
      <a href="{{url('/contactanos')}}" class="enlacespie" >CONTACTÁNOS</a>
      <img src="{{asset('/img/logo/logo-pie-anahuac.png')}}" alt="anahuac" width="52" height="52" class="centrarimagenpie">
      </center>
    </div>
  </div>
</footer>

<?php // inicio pie universal ?>
<article>

<br class="hidden-xs hidden-sm hidden-md">
<br class="hidden-xs hidden-sm hidden-md">
<br class="hidden-xs hidden-sm hidden-md">
<br class="hidden-xs hidden-sm hidden-md"><br class="hidden-xs hidden-sm hidden-md">
<br class="hidden-xs hidden-sm hidden-md">
<br class="hidden-xs hidden-sm hidden-md">
<br class="hidden-xs hidden-sm hidden-md"><br class="hidden-xs hidden-sm hidden-md">

<hr class="yellow-line">

  <footer class="elpie container-fluid hidden-xs hidden-sm">
    <div >
      <div class="row">
        <div class="col-md-4 col-sm-4">
          <!-- Por Catalán -->
          <div>

            <div id="block-footerlogo" class="block block-block-content">

              <div class="content">
                <div>
                  <p>
                    <a href="http://pegaso.anahuac.mx/radio/" target="_blank"><img alt="" src="./img/pie/redio-img.png"></a>
                  </p>
                </div>
              </div>
            </div>

          </div>

        </div>
        <div class="col-md-4 col-sm-4 bodre-right bodre-left footer-col">

          <div>

            <div id="block-campusnorthe" class="block block-block-content">

              <div class="content">

                <div>
                  <h4>CAMPUS NORTE</h4>
                  <p>Av. Universidad Anáhuac 46, Col. Lomas Anáhuac</p>
                  <p>Huixquilucan, Estado de México, C.P. 52786.</p>
                  <p>Conmutador: <strong><a href="tel:+525556270210">+52 (55) 5627 0210</a></strong></p>
                  <p>Del interior sin costo: <strong><a href="tel:+525556270210">+52 (55) 5627 0210</a></strong></p>
                  <p><a href="http://www.anahuac.mx/mexico/Avisodeprivacidad/proteccion-de-datos-personales-campus-norte" style="color:#F76F0B; "><strong>Aviso de privacidad.</strong></a></p>
                  <p>&nbsp;</p>
                  <div class="row">
                    <div class="col-md-4 col-sm-4"><span><strong>SÍGUENOS EN:</strong> </span></div>
                    <div class="col-md-8 col-sm-8">
                      <div class="social-icons">
                        <div>
                          <ul>
                            <li><a class="addthis_button_facebook" href="https://www.facebook.com/UniversidadAnahuac/"><img alt="Facebook"  src={{asset('./img/pie/facebook.png')}}></a></li>
                            <li><a class="addthis_button_twitter" href="https://twitter.com/anahuac"><img alt="Twitter"   src={{asset('./img/pie/twitter.png')}}></a></li>
                            <li><a class="addthis_button_instagram" href="https://www.instagram.com/uanahuacnorte/"><img alt="Instagram"  src={{asset('./img/pie/instagram.png')}}></a></li>
                            <li><a class="addthis_button_youtube" href="https://www.youtube.com/universidadanahuac"><img alt="Youtube"  src={{asset('./img/pie/youtube.png')}}></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>

        </div>
        <div class="col-md-4 col-sm-4 footer-col">

          <div>

            <div id="block-campussur" class="block block-block-content">
              <div class="content">
                <div>

                  <h4>CAMPUS SUR</h4>
                  <p>Av. de las Torres 131, Col. Olivar de los Padres</p>
                  <p>Ciudad de México. C.P. 01780</p>
                  <p>Conmutador: <strong><a href="tel:52555628-8800">+52 (55) 5628 8800</a></strong></p>
                  <p><a href="http://www.anahuac.mx/mexico/Avisodeprivacidad/proteccion-de-datos-personales-campus-sur" onclick="ga(&#39;uam.send&#39;, &#39;event&#39;, &#39;Footer&#39;, &#39;click&#39;, &#39;Aviso de privacidad sur&#39;);" style="color:#F76F0B; "><strong>Aviso de privacidad.</strong></a></p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <div class="row">
                    <div class="col-md-4 col-sm-4"><span><strong>SÍGUENOS EN:</strong> </span></div>
                    <div class="col-md-8 col-sm-4">
                      <div class="social-icons">
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style addthis_custom_sharing">
                          <ul>
                            <li><a class="addthis_button_facebook" href="https://www.facebook.com/AnahuacSur/"><img alt="Facebook"  src={{asset('./img/pie/facebook.png')}}></a></li>
                            <li><a class="addthis_button_twitter" href="https://twitter.com/AnahuacSur"><img alt="Twitter"  src={{asset('./img/pie/twitter_0.png')}}></a></li>
                            <li><a class="addthis_button_instagram" href="https://www.instagram.com/uanahuacsur/"><img alt="Instagram"  src={{asset('./img/pie/instagram.png')}}></a></li>
                            <li><a class="addthis_button_youtube" href="https://www.youtube.com/universidadanahuac"><img alt="Youtube"  src={{asset('./img/pie/youtube.png')}}></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
      <div class="row" style="margin-top:50px;">
        <div class="col-lg-12 col-sm-12 bottom-footer">

          <div>

            <div id="block-footercopyright" class="block block-block-content">

              <div class="content">

                <div>
                  <div class="logos-footer">
                    <img alt="Fimpes" src={{asset('./img/pie/fimpes_0.png')}} class="align-center">&nbsp;<img alt="ANUIES" src="./img/pie/anuies_0.png" class="align-center"><img alt="SEP" class="align-center" src={{asset('./img/pie/sep_0.png')}}>&nbsp;
                    <img alt="Empresa Socialmente Responsable" src={{asset('./img/pie/esr.png')}} class="align-center">
                    <img alt="Empresa Familiarmente Responsable" src={{asset('./img/pie/efr.png')}} class="align-center">
                    <p>&nbsp;</p>
                    <h3>Universidad Anáhuac 2017. Todos los Derechos Reservados.</h3>
                    <p>&nbsp;</p>
                  </div>
                </div>

              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  </footer>
</article>
