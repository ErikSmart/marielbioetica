<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bióetica</title>
        <link rel="shortcut icon" href="{{ asset('//img/logo.gif')}}" />
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{ asset('/css/main.css') }}" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <!-- iCheck -->
        <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
        <!-- select2 -->
        <link rel="stylesheet" href="{{ asset('/plugins/select2/select2.min.css') }}" >
        <link rel="stylesheet" href="{{ asset('/css/jquery-confirm.css')}}">
        <link rel="stylesheet" href="{{ asset('/js/slick/slick.css')}}">
        <link rel="stylesheet" href="{{ asset('/js/slick/slick-theme.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/menu.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/ocultar-menu.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/menu-principal.css')}}">
    </head>

    <body data-spy="scroll" data-offset="0" data-target="#navigation" id="capaRespuesta" >

     <?php // menu principal?>
     <a href="#main-content" class="visually-hidden focusable">Pasar al contenido principal</a>

     <header class="container-fluid fondo">
     <div class="container">
     <div class="row margin20 logo-areas">
     <!-- Inicio logo -->
     <div class="col-lg-3 col-sm-3 log-izq">
     <div>
       <div id="block-logo" class="block block-block-content ">
         <div class="content">
           <div>
             <p><a href="http://www.anahuac.mx/mexico"><img alt="Universidad Anáhuac México"   src="{{ asset('/img/logo_0.png')}}"></a></p>
           </div>
         </div>
       </div>
     </div>
     </div>
     <!-- Fin logo -->
     <!-- Inicio Slogan -->
     <div class="col-lg-6 col-sm-6 frase-arriba">
     <div>
       <div id="block-headercenter" class="block block-block-content ">
         <div class="content">
           <div>
             <h3>Somos Anáhuac México. Líderes de Acción Positiva.</h3>
           </div>
         </div>
       </div>
     </div>
     </div>
     <!-- Fin slogan -->
     <div class="col-lg-3 col-sm-3 idiomas-der"></div>
     </div>
     </div>
     </header>
     <!-- Fin encabezado -->
     <!-- Por Catalán -->
     <!-- Inicio del menú principal -->
     <nav class="navbar navbar-default navbar-static-top navbar-yellow">
     <div class="container">
     <div class="navbar-header">
     <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
     </div>
     <div class="navbar-collapse collapse">
     <div>

     <nav role="navigation" aria-labelledby="block-mainnavigation-menu" id="block-mainnavigation">
       <h2 class="visually-hidden" id="block-mainnavigation-menu">Main navigation</h2>
       <ul class="nav navbar-nav">
         <li>
           <a href="http://www.anahuac.mx/mexico/"  class="is-active">Inicio</a>
         </li>
         <?php // Inicio del menú de bioetica ?>
         <li>
           <a href="{{url('/mapa')}}" target=""  class="hidden-md hidden-lg" >MAPA</a>
           </li>
           <li>
             <a href="{{url('/quienes-somos')}}" target=""  class="hidden-md hidden-lg" >Quienes Somos</a>
             </li>
             <li>
               <a href="{{url('/areas-de-bioetica')}}" target=""  class="hidden-md hidden-lg" >Áreas de bioetica</a>
               </li>
               <li >
                 <a href="{{url('/unete')}}" target=""  class="hidden-md hidden-lg">Únete</a>
                 </li>
          <?php // Fin del menú de bioetica ?>         
         <li class="has-child">
           <a href="http://www.anahuac.mx/mexico/" target=""  class="is-active dropdown-toggle" data-toggle="dropdown">NUESTRA UNIVERSIDAd</a>
           <ul class="dropdown-menu" >
             <li>
               <a href="http://www.anahuac.mx/mexico/mision" target="_self" title="Misión">Misión</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/historia" target="" title="Historia">Historia</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/Modelo-Educativo-Anahuac-de-Formacion-Integral" target="" title="Modelo Anáhuac">Modelo Educativo Anáhuac</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/ObjetivosFormativos" target="" title="Objetivos Formativos">Objetivos Formativos</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/calidad-academica-acreditada" target="" title="Calidad Académica">Calidad Académica Acreditada</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/lineas-estrategicas" target="" title="Líneas Estratégicas">Líneas Estratégicas</a>
             </li>
             <li>
               <a href="http://anahuac.mx/mexico/files/2017/ManualdeImagenInstitucional.pdf" target="_blank" title="Manual de Imagen Institucional">Manual de Imagen Institucional</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/NuestrosCampus" target="" title="Nuestros Campus">Nuestros Campus</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/Directorio" target="" title="Directorio de áreas de la Universidad Anáhuac México">Directorio</a>
             </li>
             <li>
               <a href="http://redanahuac.mx/rua/" target="_blank" title="Red de Universidades Anáhuac">Red de Universidades Anáhuac</a>
             </li>
           </ul>

         </li>
         <li class="has-child">
           <a href="http://pegaso.anahuac.mx/preuniversitarios/oferta-educativa" target="_blank" class="dropdown-toggle" data-toggle="dropdown">LICENCIATURAS</a>
           <ul class="dropdown-menu" >
             <li>
               <a href="http://www.anahuac.mx/mexico/preuniversitarios/" target="_blank" title="inicio">Conoce más...</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/preuniversitarios/proceso-de-admision" target="_blank" title="Admisiones">Admisiones</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/preuniversitarios/oferta-educativa" target="_blank" title="Oferta Académica">Oferta Académica</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/preuniversitarios/cuotas" target="_blank" title="Costos">Costos</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/preuniversitarios/financiamiento" target="_blank" title="Becas y Financiamiento">Becas y Financiamiento</a>
             </li>
             <li>
               <a href="http://ww2.anahuac.mx/foraneos/" target="_blank" title="Alumnos Foráneos">Alumnos Foráneos</a>
             </li>
           </ul>

         </li>
         <li class="has-child">
           <a href="http://www.anahuac.mx/mexico/posgrados/" target="_blank" class="dropdown-toggle" data-toggle="dropdown">POSGRADOS</a>
           <ul class="dropdown-menu" >
             <li>
               <a href="http://www.anahuac.mx/mexico/posgrados/" target="_blank">Conoce más...</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/posgrados/admisiones" target="_blank" title="Admisiones">Admisiones</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/posgrados/escuelas-y-facultades" target="_blank" title="Oferta Académica">Oferta Académica</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/posgrados/costos-y-financiamiento" target="_blank" title="Costos">Costos y Financiamiento</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/posgrados/convenios-de-descuento" target="_blank" title="Becas y Financiamiento">Becas y Financiamiento</a>
             </li>
           </ul>

         </li>
         <li class="has-child">
           <a href="http://www.anahuac.mx/mexico/educacioncontinua/" target="_blank" title="EDUCACIÓN CONTINUA" class="dropdown-toggle" data-toggle="dropdown">EDUCACIÓN CONTINUA</a>
           <ul class="dropdown-menu" >
             <li>
               <a href="http://www.anahuac.mx/mexico/educacioncontinua/" target="_blank">Conoce más...</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/educacioncontinua/admisiones" target="_blank" title="Admisiones">Admisiones</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/educacioncontinua/programas" target="_blank" title="Oferta Académica">Oferta Académica</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/educacioncontinua/costos-y-procesos-de-pago" target="_blank" title="Costos">Costos y Proceso de Pago</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/educacioncontinua/convenios-de-descuento" target="_blank" title="Becas y Financiamiento">Becas y Financiamiento</a>
             </li>
           </ul>

         </li>
         <li>
           <a href="http://www.anahuac.mx/mexico/VidaUniversitaria" target="" >VIDA UNIVERSITARIA</a>
         </li>
         <li>
           <a href="http://www.anahuac.mx/mexico/EscuelasyFacultades" target="" >ESCUELAS Y FACULTADES</a>
         </li>
       </ul>
     </nav>
     </div>
     </div>
     </div>
     </nav>
     <!-- Fin del menú principal -->

     <!-- Inicio menú secundario -->
     <nav class="navbar second-nav-bar navbar-static-top navbar-yellow">
     <div class="container">
     <div class="navbar-header">
     <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
     </div>
     <div class="navbar-collapse collapse">

     <div>
     <nav role="navigation" aria-labelledby="block-secondarymenu-menu" id="block-secondarymenu">
       <h2 class="visually-hidden" id="block-secondarymenu-menu">Secondary Menu</h2>

       <ul class="nav navbar-nav">
         <li class="has-child">
           <a href="http://ww2.anahuac.mx/rectoria/" target="_blank" title="OFICINA DE RECTORÍA" class="dropdown-toggle" data-toggle="dropdown">OFICINA DE RECTORÍA</a>
           <ul class="dropdown-menu" >
             <li>
               <a href="http://ww2.anahuac.mx/rectoria/rector/mensaje-del-rector" target="_blank" title="Mensaje del Rector">Mensaje del Rector</a>
             </li>
             <li>
               <a href="http://ww2.anahuac.mx/rectoria/rector/semblanza" target="_blank" title="Semblanza">Semblanza</a>
             </li>
             <li>
               <a href="http://ww2.anahuac.mx/rectoria/" target="_blank" title="Discursos">Discursos</a>
             </li>
             <li>
               <a href="http://www.anahuac.mx/mexico/2016/Rector&#39;s-annual-report.pdf" target="_blank" title="Informe del 2016">Informe 2016</a>
             </li>
           </ul>

         </li>
         <li>
           <a href="http://online.anahuac.mx/" target="_self" title="Oferta maestrías en línea">MAESTRÍAS EN LÍNEA</a>
         </li>
         <li>
           <a href="http://www.anahuac.mx/mexico/ServiciosUniversitarios" target="" title="SERVICIOS UNIVERSITARIOS">SERVICIOS UNIVERSITARIOS</a>
         </li>
         <li>
           <a href="http://pegaso.anahuac.mx/egresados" target="_blank" title="EGRESADOS">EGRESADOS</a>
         </li>
         <li>
           <a href="http://www.anahuac.mx/mexico/noticias" target="" title="NOTICIAS" >NOTICIAS</a>
         </li>
       </ul>
     </nav>
     </div>
     </div>
     </div>
     </nav>
     <div>
     <!-- Fin menú secundario -->
     <?php // fin ?>


        <div id="preloadDiv" class="text-center" style="position:fixed;top:40%;left:0;right:0;padding-bottom:0;z-index:100;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw text-orange" style="font-size: 50px"></i>
        </div>

        <!--=================MENU====================-->
        <div class="container-fluid full" >
            <header class="row" style="margin-right:0 ">
                <div class="col-lg-3 col-md-5 col-sm-6 col-xs-12" style="padding-right: 0;">
                    <a href="{{url('/')}}"><img src="{{asset('/img/logo/logo-anahuac.png')}}" alt="logo de anahuac"  class="img-responsive" ></a>
                </div>
                @if(isset($title) && !empty($title))
                <div class="col-lg-8 col-md-8 hidden-md hidden-sm hidden-xs namePage" > {{$title}} <center style="padding-top: 15px;"><hr class="linea"></center></div>
                @endif
            </header>
            <div class="row" style="margin-right:0 ">
<nav class="contenerdorbotones col-sm-3 menuShow hidden-md hidden-xs">

<a href="{{url('/mapa')}}" class="boton-navegacion " @if(isset($Mactive) && !empty($Mactive))style="{{$Mactive}}"@endif  title="Encuentra un profecional en bioética">MAPA</a>


<a href="{{url('/quienes-somos')}}" class="boton-navegacion "@if(isset($Qactive) && !empty($Qactive))style="{{$Qactive}}"@endif  title="Quiénes somos los de área de bioética">QUIÉNES<br>SOMOS</a>


<a href="{{url('/areas-de-bioetica')}}" class="boton-navegacion "@if(isset($Bactive) && !empty($Bactive))style="{{$Bactive}}"@endif  title="Áreas disponibles del la bioética">ÁREAS DE <br>LA BIÓETICA</a>


<a href="{{url('/unete')}}" class="boton-navegacion " @if(isset($Uactive) && !empty($Uactive))style="{{$Uactive}}"@endif  title="Formulario registrate en bioética">ÚNETE</a>

</nav>
<div style="padding-bottom: 20px">
@yield('content')
</div>


            </div>
            <!-- footer -->
            @include('cuerpo.footer')
            <!-- end footer -->
        </body>
        <!-- jQuery 2.1.4 -->
        <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset('/js/push.min.js')}}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
        <!-- selct2 -->
        <script src="{{ asset('/plugins/select2/select2.min.js') }}"></script>
        <!-- validaciones de boostrap -->
        <script src="{{ asset('/js/formularioUsuario.js') }}"></script>
        <script src="{{ asset('/js/bootstrapValidator.min.js') }}"></script>
        <script src="{{ asset('/js/sendformu.js')}}"></script>
        <!-- buscador de palabras  -->
        <script src="{{ asset('/js/busqueda.js')}}"></script>
        <!-- dar de alta o reincoporar el usuario -->
        <script src="{{ asset('/js/accionesUser.js')}}"></script>
        <script src="{{ asset('/js/jquery-confirm.js')}}"></script>
        <script src="{{ asset('/js/slick/slick.js')}}"></script>
        <script src="{{ asset('/js/captcha.js')}}"></script>
        <script src="{{ asset('/js/inicial.js')}}"></script>
        <script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('/js/historialCookies.js')}}"></script>

        <script src="{{ asset('/js/sendMail.js')}}"></script>
        <script src="{{ asset('/js/menu-script.js')}}"></script>
    </html>
