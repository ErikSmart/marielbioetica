<?php

use Illuminate\Database\Seeder;

class vinculacionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('referentials')->insert([
          'nombreCompleto'=>'Martha Tarasco Michel',
          'id_perfil'=>2,
          'id_status_formacion'=>5,
          'formAcademicLIC'=>'Medicina',
          'id_area_LIC'=>1,
          'formdeMaestria'=>'Investigacion clinica',
          'formEspecialidad'=>'Medicina de la comunicación humana',
          'formDoctorado'=>'Medicina',
          'formPostGrado'=>null,
          'id_status_gestion_laboral'=>8,
          'id_status_form_docencia'=>8,
          'id_status_invest_bio'=>8,
          'investigacion_temas'=>null,
          'telefono'=>'56270211',
          'telefono2'=>'',
          'correo'=>''mtarascos@anahuac.mx'',
          'correo2'=>'',
          'palabras_claves'=>'Antropologia_filosogica, epistemologia,conducta',
          'id_respuestas_gestion_laboral'=>1,
          'id_respuestas_form_docencia'=>2,
          ],[
           'nombreCompleto'=> 'Mariel Kalkach Aparicio',
           'id_perfil'=> '3',
           'id_status_formacion'=> '6',
           'formAcademicLIC'=> 'Medicina',
           'id_area_LIC'=> '1',
           'formdeMaestria'=> 'Bioetica',
           'formEspecialidad'=> 'Bioetica',
           'formDoctorado'=> '',
           'formPostGrado'=> NULL,
           'id_status_gestion_laboral'=> 8,
           'id_status_form_docencia'=> 8,
           'id_status_invest_bio'=> 8,
           'investigacion_temas'=>  NULL,
           'telefono'=> '555-5555555',
           'telefono2'=> '',
           'correo'=>  'mariel.kalkac@gmail.com',
           'correo2'=>  '',
           'palabras_claves'=>  'neuroetica,conciencia,neurobioetica',
           'id_respuestas_gestion_laboral'=>  '13',
           'id_respuestas_form_docencia'=>  '14',
           'created_at'=>  '2017-03-18 06:00:00',
           'updated_at'=>  NULL,
           'deleted_at'=>  NULL)
          ]);
    }
}
