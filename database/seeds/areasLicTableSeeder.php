<?php

use Illuminate\Database\Seeder;

class areasLicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('areasLic')->insert([ 
            'nombreArea' => 'Ciencias de la Salud. (medicina, nutrición,psicología, enfermería, rehabilitación, etc.)',   
            'acronimo'     =>'C. DE LA SALUD',
            'color'    	=>'765AA3',
        ]);
        DB::table('areasLic')->insert([ 
            'nombreArea' => 'Ciencias Sociales y Humanidades (filosofía, historia, derecho, etc.)',
            'acronimo'     =>'C. SOCIALES Y HUMANIDADES',         
            'color'     =>'76BBA5',       	
        ]);
        DB::table('areasLic')->insert([ 
            'nombreArea' => 'Ciencias Económicas (administración, economía, contaduría, actuaría, etc.)',
            'acronimo'     =>'C. ECONÓMICAS',
            'color'     =>'C37E2B',       	
        ]);
        DB::table('areasLic')->insert([ 
            'nombreArea' => 'Ciencias Naturales (biología, química, física, geografía, astronomía, etc.)',
            'acronimo'     =>'C. NATURALES',
            'color'     =>'A5B81E',       	
        ]);
        DB::table('areasLic')->insert([ 
            'nombreArea' => 'Ciencias Religiosas (teología, filosofía, etc.)',
            'acronimo'     =>'C. NATURALES',
            'color'     =>'E2C273',       	
        ]);
        DB::table('areasLic')->insert([ 
            'nombreArea' => 'Ciencias Tecnológicas (Biotecnología, Ing. Industrial, Ing. Mecánica, Ing. Informática, etc.)',
            'acronimo'     =>'C. TECNOLÓGICAS',
            'color'     =>'1F227E',       	
        ]);
        DB::table('areasLic')->insert([ 
            'nombreArea' => 'Arte y Cultura (Danza, Teatro, Fotografía, Diseño)',   
            'acronimo'     =>'ARTE Y CULTURA',
            'color'     =>'7F2338',         
        ]);
          DB::table('areasLic')->insert([ 
            'nombreArea' => 'Otra área',
            'acronimo'     =>'OTROS',
            'color'     =>'9F3889',       	
        ]);
    }
}
