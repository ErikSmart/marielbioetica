<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registris', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('author')->unsigned();
            $table->integer('addressee')->unsigned();
            $table->timestamps();
            
            $table->foreign('author')->references('id')->on('users');
            $table->foreign('addressee')->references('id')->on('vinculacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registris');
    }
}
