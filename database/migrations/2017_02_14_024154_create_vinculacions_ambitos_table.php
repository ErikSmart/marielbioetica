<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVinculacionsAmbitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vinculacions_ambitos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_vinculacions'); 
            $table->integer('id_ambitos');
            $table->integer('id_tipo');
            $table->boolean('cheked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vinculacions_ambitos');
    }
}
